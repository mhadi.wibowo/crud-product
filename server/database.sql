CREATE DATABASE jubelio;

CREATE TABLE product(
  sku SERIAL PRIMARY KEY,
  nama VARCHAR(50) NOT NULL,
  harga INT,
  gambar VARCHAR(255),
  description VARCHAR(255)
);