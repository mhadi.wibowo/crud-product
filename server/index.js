const express = require('express');
const app = express();
const cors = require('cors');
const pool = require('./db');

//middleware
app.use(cors());
app.use(express.json());

//ROUTES//

// create a product
app.post('/product', async (req, res) => {
  try {
    const { nama } = req.body;
    const newProduct = await pool.query(
      'INSERT INTO product (nama) VALUES($1) RETURNING *',
      [nama]
    );

    res.json(newProduct.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
});

// get all product
app.get('/product', async (req, res) => {
  try {
    const allProduct = await pool.query('SELECT * FROM product');

    res.json(allProduct.rows);
  } catch (err) {
    console.error(err.message);
  }
});

// get a product
app.get('/product/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const product = await pool.query('SELECT * FROM product WHERE sku = $1', [id])

    res.json(product.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
});

// update a product
app.put('/product/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { nama } = req.body;
    const updateProduct = await pool.query('UPDATE product SET nama = $1 WHERE sku = $2', [nama, id])

    res.json('Product was updated!')
  } catch (err) {
    console.error(err.message);
  }
});

// delete a product
app.delete('/product/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const deleteProduct = await pool.query('DELETE FROM product WHERE sku = $1', [id])

    res.json('Product was deleted!');
  } catch (err) {
    console.error(err.message);
  }
});

app.listen(5000, () => {
  console.log('server has started on port 5000');
});