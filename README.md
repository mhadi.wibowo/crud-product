# CRUD Product

## How to run project

1.  Clone the repository and go to the project folder on your terminal

    ```
    https://gitlab.com/mhadi.wibowo/crud-product
    ```

2.  Install dependencies

    ```
    npm install
    ```

3.  Database

    Config the database and make it match to your postgres user on the config file at:

    ```
    db.js
    ```

4.  Run the project at terminal:

    ```
    cd server
    node index.js
   
    cd ../client
    npm start
    ```

5.  With browser, access the project at `http://localhost:3000`

---

## Available Endpoints

|  Endpoint   | Method |    Description    |
| :---------  |:-------|   :------------   |
| /product    | GET    |  Get all products |
| /product/id | GET    |  Get a product    |
| /product/id | POST   |  Create a product |
| /product/id | PUT    |  Update a product |
| /product/id | DELETE |  Delete a product |

