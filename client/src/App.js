import React, { Fragment } from 'react';

//components

import Header from './components/Header';
import InputProduct from './components/InputProduct';
import ListProduct from './components/ListProduct';

function App() {
  return <Fragment>
    <div className='container'>
      <Header />
      <InputProduct />
      <ListProduct />
    </div>
  </Fragment>
}

export default App;
