import React, { Fragment, useEffect, useState } from 'react';
import EditProduct from './EditProduct';

const ListProduct = () => {
  const [product, setProduct] = useState([]);
  const deleteProduct = async (id) => {
    try {
      const deleteProduct = await fetch(`http://localhost:5000/product/${id}`, {
        method: "DELETE"
      });

      setProduct(product.filter(product => product.sku !== id));
    } catch (err) {
      console.error(err.message);
    }
  }
  const getProduct = async () => {
    try {

      const response = await fetch('http://localhost:5000/product')
      const jsonData = await response.json();

      setProduct(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  }

  useEffect(() => {
    getProduct();
  }, []);

  return (
    <Fragment>
      {''}
      <table class="table mt-5 text-center">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Deskripsi</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {product.map(product => (
            <tr key={product.sku}>
              <td>{product.nama}</td>
              <td>{product.harga}</td>
              <td>{product.description}</td>
              <td>
                <EditProduct product={product} />
              </td>
              <td><button className='btn btn-danger' onClick={() => deleteProduct(product.sku)}>Delete</button></td>
            </tr>
          ))}
        </tbody>
      </table >
    </Fragment >
  )
}

export default ListProduct;