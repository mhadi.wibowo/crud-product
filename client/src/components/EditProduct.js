import React, { Fragment, useState } from "react";

const EditProduct = ({ product }) => {
  const [nama, setNama] = useState(product.nama)
  const [harga, setHarga] = useState(product.harga)
  const [description, setDescription] = useState(product.description)

  // Edit Description Function
  const updateNama = async (e) => {
    e.preventDefault();
    try {
      const body = { nama };
      const response = await fetch(`http://localhost:5000/product/${product.sku}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json ' },
        body: JSON.stringify(body)
      });

      window.location = '/';
    } catch (err) {
      console.error(err.message);
    }
  }


  return (
    <Fragment>
      <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target={`#id${product.sku}`}>
        Edit
      </button>

      <div class="modal" id={`id${product.sku}`}>
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <h4 class="modal-title">Edit Product</h4>
              <button type="button" class="btn-close" data-bs-dismiss="modal" onClick={() => setNama(product.nama)}></button>
            </div>

            <div class="modal-body">
              <input type="text" className="form-control" value={nama} onChange={e => setNama(e.target.value)} />
              <input type="text" className="form-control mt-3" value={harga} onChange={e => setHarga(e.target.value)} />
              <input type="text" className="form-control mt-3" value={description} onChange={e => setDescription(e.target.value)} />
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-warning" data-bs-dismiss="modal" onClick={e => updateNama(e)}>Edit</button>
              <button type="button" class="btn btn-danger" data-bs-dismiss="modal" onClick={() => setNama(product.nama)}>Close</button>
            </div>

          </div>
        </div>
      </div>
    </Fragment>
  )
};

export default EditProduct;