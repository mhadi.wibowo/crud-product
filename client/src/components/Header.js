import React, { Fragment } from 'react';

const Header = () => {
  return (
    <Fragment>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
          {/* <a class="navbar-brand" href="#">
            <img src="img/logo.png" width="120">
          </a> */}
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link active" aria-current="page" href="#">Dashboard</a>
            </div>
          </div>
        </div>
      </nav>

      <div class="container">
        <div class="row mt-3">
          <div class="col">
            <h1>List Product</h1>
          </div>
        </div>

        <div class="row" id="daftar-menu">

        </div>

      </div>
    </Fragment>
  )
}

export default Header;