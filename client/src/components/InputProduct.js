import React, { Fragment, useState } from 'react';

const InputProduct = () => {

  const [nama, setNama] = useState('Nama')
  const [harga, setHarga] = useState('Harga')
  const [description, setDescription] = useState('Deskripsi')

  const onSubmitForm = async e => {
    e.preventDefault();
    try {
      const body = { nama };
      const response = await fetch('http://localhost:5000/product', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
      });

      window.location = '/';
    } catch (err) {
      console.error(err.message);
    }
  }

  return (
    <Fragment>
      <h2 className='text-center mt-5'>Add Product</h2>
      <form className='d-flex mt-3' onSubmit={onSubmitForm}>
        <input type='text' className='form-control' value={nama} onChange={e => setNama(e.target.value)} />
        <input type='text' className='form-control' value={harga} onChange={e => setHarga(e.target.value)} />
        <input type='text' className='form-control' value={description} onChange={e => setDescription(e.target.value)} />
        <button className='btn btn-success'>Add</button>
      </form>
    </Fragment>
  )
}

export default InputProduct;